using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class ExitLevel : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 1f;
    BoxCollider2D DoorExit;

    void Start()
    {
        DoorExit = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        Exit();
    }


    void Exit()
    {
        if (DoorExit.IsTouchingLayers(LayerMask.GetMask("Player")))
        {

            StartCoroutine(LoadNextLevel());

        }

        IEnumerator LoadNextLevel()
        {
            yield return new WaitForSecondsRealtime(levelLoadDelay);
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            int nextSceneIndex = currentSceneIndex + 1;

            if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
            {
                nextSceneIndex = 0;
            }
            FindObjectOfType<ScenePersist>().ResetScenePersist();
            SceneManager.LoadScene(nextSceneIndex);
        }


            
            
        
    }



}
