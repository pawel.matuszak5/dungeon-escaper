using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{



    Rigidbody2D myRigidbody;
    Animator myAnimator;
    CapsuleCollider2D PlayerIsNeer;


    bool isNeer = false;

    // Start is called before the first frame update
    void Start()
    {
        PlayerIsNeer = GetComponent<CapsuleCollider2D>();
        myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerNeer();
    }

    void PlayerNeer()
    {

        if (PlayerIsNeer.IsTouchingLayers(LayerMask.GetMask("Player")))
        {
            myAnimator.SetBool("PlayerIsNeer", true);
            
        }


    }
}
